package com.tmckdevelopment.gyroscop.objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;

import com.tmckdevelopment.gyroscop.activity.MainActivity;
import com.tmckdevelopment.gyroscop.fragments.FinalFragment;

import java.util.ArrayList;

/**
 * Created by Tomas on 14.07.2016.
 */
public class Level {

    private int background, ballRes, holeRes;
    private String time;
    private Bitmap bitmap;
    private ArrayList<Barrier> barriers = new ArrayList<>();
    private boolean showInForeground;

    public boolean isShowInForeground() {
        return showInForeground;
    }

    public void setShowInForeground(boolean showInForeground) {
        this.showInForeground = showInForeground;
    }

    public ArrayList<Barrier> getBarriers() {
        return barriers;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public void setBallRes(int ballRes) {
        this.ballRes = ballRes;
    }

    public void setHoleRes(int holeRes) {
        this.holeRes = holeRes;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getBackground() {
        return background;
    }

    public int getBallRes() {
        return ballRes;
    }

    public int getHoleRes() {
        return holeRes;
    }

    public String getTime() {
        return time;
    }

    public void drawBarriers(Canvas canvas,float ballX,float ballY,Context context,int ballSize){

        for(int i = 0; i < barriers.size(); i++){
            barriers.get(i).drawBarrier(ballX, ballY, canvas, context,ballSize,bitmap);
        }

    }

    public void setBarriers(Context context,Canvas canvas){

        Log.d("SIZESS",String.valueOf(canvas.getWidth())+" x "+String.valueOf(canvas.getHeight()));
        for(int i = 0; i < barriers.size(); i++){
            bitmap = BitmapFactory.decodeResource(context.getResources(), barriers.get(i).getBarrierRes());

            Log.d("SIZ",String.valueOf(String.valueOf((canvas.getWidth()/(canvas.getWidth()/3.5))))+" x "+String.valueOf((canvas.getHeight()/(canvas.getHeight()/3.5))));

            barriers.get(i).setBarrierImage((Bitmap.createScaledBitmap(bitmap,(int)((bitmap.getWidth()/(canvas.getWidth()/(canvas.getWidth()/3.5)))),(int)(bitmap.getHeight()/(canvas.getHeight()/(canvas.getHeight()/3.5)))
                    , false)));
        }
    }

    public void drawBall(Canvas canvas, Bitmap ballImage, float ballX, float ballY) {
        canvas.drawBitmap(ballImage, ballX, ballY, null);
    }

    public void drawHole(Canvas canvas, float ballX, float ballY, int ballSize, Context context, Bitmap holeImage) {

        float holeX = canvas.getWidth() - (ballSize * 3);
        float holeY = canvas.getHeight() - (ballSize * 3);

        if (ballX > holeX + (ballSize / 3) && ballY > holeY + (ballSize / 3) &&
                ballX + (ballSize/3) < holeX + (ballSize) && ballY + (ballSize/3)  < holeY + (ballSize)) {
            ((MainActivity)context).setHasWon(true);
            ((MainActivity) context).showFragment(FinalFragment.newInstance(), FinalFragment.TAG, false);
        }
        canvas.drawBitmap(holeImage, holeX, holeY, null);
    }
}
