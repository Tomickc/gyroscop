package com.tmckdevelopment.gyroscop.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.tmckdevelopment.gyroscop.R;
import com.tmckdevelopment.gyroscop.activity.MainActivity;

/**
 * Created by Tomas on 14.07.2016.
 */
public class BaseFragment extends Fragment {

    public void showFragment(Fragment fragment, String tag, boolean backStack) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        ((MainActivity)getActivity()).setCurrentFragment(tag);
        transaction.replace(R.id.fragment, fragment, tag);
        if (backStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

}
