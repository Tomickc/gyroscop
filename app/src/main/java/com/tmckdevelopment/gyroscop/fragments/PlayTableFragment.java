package com.tmckdevelopment.gyroscop.fragments;

/**
 * Created by Tomas on 14.07.2016.
 */

import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.tmckdevelopment.gyroscop.R;
import com.tmckdevelopment.gyroscop.activity.MainActivity;
import com.tmckdevelopment.gyroscop.view.Chronometer;
import com.tmckdevelopment.gyroscop.view.PlayTable;

public class PlayTableFragment extends BaseFragment {
    //Set fragment's TAG
    public static final String TAG = "playtable_fragment";

    public Chronometer chronometer;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playtable, container, false);

        //create new instance of playTable
        PlayTable playTable = new PlayTable(getActivity());

        chronometer = (Chronometer) view.findViewById(R.id.chronometer);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
        ImageView background = (ImageView) view.findViewById(R.id.imageViewBackground);
        ImageView restart = (ImageView) view.findViewById(R.id.buttonRestartInGame);

        Glide.with(getActivity()).load(((MainActivity) getActivity()).getLevel().getBackground()).centerCrop().into(background);

        linearLayout.addView(playTable);

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFragment(newInstance(), TAG, false);
            }
        });

        chronometer.start();

        return view;
    }

    //Create new instance of this fragment
    public static PlayTableFragment newInstance() {
        PlayTableFragment fragment = new PlayTableFragment();
        return fragment;
    }


    @Override
    public void onPause() {
        super.onPause();
        chronometer.stop();
        ((MainActivity) getActivity()).getLevel().setTime(String.valueOf((float) (SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000));
    }
}