package com.tmckdevelopment.gyroscop.objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;

import com.tmckdevelopment.gyroscop.activity.MainActivity;
import com.tmckdevelopment.gyroscop.fragments.FinalFragment;

/**
 * Created by Tomas on 16.07.2016.
 */
public class Barrier {

    public int barrierX, barrierY, barrierRes, speed;
    public Bitmap barrierImage;
    public boolean moveUp, moveDown, moveLeft, moveRight,sensitive;


    public void sensitive(boolean sensitive) {
        this.sensitive = sensitive;
    }

    public void moveLeft(boolean moveLeft) {
        this.moveLeft = moveLeft;
    }

    public void moveRight(boolean moveRight) {
        this.moveRight = moveRight;
    }

    public void moveDown(boolean moveDown) {
        this.moveDown = moveDown;
    }

    public void moveUp(boolean moveUp) {
        this.moveUp = moveUp;
    }

    public void setSpeed(int speed,int height,int width) {
        this.speed = (width+height)/speed;
        Log.d("SPEED",String.valueOf(this.speed));
    }

    public void setBarrierImage(Bitmap barrierImage) {
        this.barrierImage = barrierImage;
    }

    public int getBarrierRes() {
        return barrierRes;
    }

    public void setBarrierPosition(int barrierX, int barrierY) {
        this.barrierY = barrierY;
        this.barrierX = barrierX;
    }


    public void setBarrierRes(int barrierRes) {
        this.barrierRes = barrierRes;
    }

    public void drawBarrier(float ballX, float ballY, Canvas canvas, Context context, int ballSize, Bitmap bitmap) {

        if (moveUp) moveUp(canvas);
        if (moveDown) moveDown(canvas);
        if (moveLeft) moveLeft(canvas);
        if (moveRight) moveRight(canvas);

        canvas.drawBitmap(barrierImage, barrierX, barrierY, null);

        if(sensitive){
            if (ballX+(ballSize*0.8) > barrierX && ballY+(ballSize*0.8) > barrierY &&
                    ballX < barrierX + ((int) (barrierImage.getWidth()))-(ballSize*0.2) && ballY  < barrierY + ((barrierImage.getHeight())-(ballSize*0.2))) {
                ((MainActivity) context).setHasWon(false);
                ((MainActivity) context).showFragment(FinalFragment.newInstance(), FinalFragment.TAG, false);
            }
        }else {

            if (ballX > barrierX && ballY > barrierY &&
                    ballX + ballSize < barrierX + ((int) (barrierImage.getWidth())) && ballY + ballSize < barrierY + ((barrierImage.getHeight()))) {
                ((MainActivity) context).setHasWon(false);
                ((MainActivity) context).showFragment(FinalFragment.newInstance(), FinalFragment.TAG, false);
            }
        }
    }

    private void moveLeft(Canvas canvas) {

        if (barrierX < -(barrierImage.getWidth())) {
            barrierX = canvas.getWidth();
        } else {
            barrierX -= speed;
        }
    }

    private void moveRight(Canvas canvas) {

        if (barrierX > canvas.getWidth() + (barrierImage.getWidth())) {
            barrierX = -(barrierImage.getWidth());
        } else {
            barrierX += speed;
        }
    }

    public void moveUp(Canvas canvas) {

        if (barrierY < -(barrierImage.getHeight())) {
            barrierY = canvas.getHeight();
        } else {
            barrierY -= speed;
        }
    }

    public void moveDown(Canvas canvas) {

        if (barrierY > canvas.getHeight() + (barrierImage.getHeight())) {
            barrierY = -(barrierImage.getHeight());
        } else {
            barrierY += speed;
        }
    }
}
