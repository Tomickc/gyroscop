package com.tmckdevelopment.gyroscop.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.tmckdevelopment.gyroscop.R;
import com.tmckdevelopment.gyroscop.activity.MainActivity;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tomas on 14.07.2016.
 */
public class MenuFragment extends BaseFragment {
    //Set fragment's TAG
    public static final String TAG = "menu_fragment";

    private CircleImageView buttonLevel1,buttonLevel2,buttonLevel3,buttonLevel4,buttonLevel5,buttonLevel6,buttonLevel7,buttonLevel8;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        defineViews(view);
        loadImages();

        buttonLevel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setLevel1();
            }
        });

        buttonLevel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setLevel2();
            }
        });

        buttonLevel3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setLevel3();
            }
        });
        buttonLevel4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setLevel4();
            }
        });

        buttonLevel5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setLevel5();
            }
        });

        buttonLevel6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setLevel6();
            }
        });

        buttonLevel7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setLevel7();
            }
        });
        buttonLevel8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setLevel8();
            }
        });
        return view;
    }

    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        return fragment;
    }

    private void defineViews(View view){
        buttonLevel1 = (CircleImageView)view.findViewById(R.id.buttonLevelOne);
        buttonLevel2 = (CircleImageView)view.findViewById(R.id.buttonLevelTwo);
        buttonLevel3 = (CircleImageView)view.findViewById(R.id.buttonLevelThree);
        buttonLevel4 = (CircleImageView)view.findViewById(R.id.buttonLevelFour);
        buttonLevel5 = (CircleImageView)view.findViewById(R.id.buttonLevelFive);
        buttonLevel6 = (CircleImageView)view.findViewById(R.id.buttonLevelSix);
        buttonLevel7 = (CircleImageView)view.findViewById(R.id.buttonLevelSeven);
        buttonLevel8 = (CircleImageView)view.findViewById(R.id.buttonLevelEight);
    }

    private void loadImages(){
        Glide.with(this).load(R.drawable.background_paper).into(buttonLevel1);
        Glide.with(this).load(R.drawable.background_aqua).into(buttonLevel2);
        Glide.with(this).load(R.drawable.background_slither).into(buttonLevel3);
        Glide.with(this).load(R.drawable.background_grass).into(buttonLevel4);
        Glide.with(this).load(R.drawable.background_flappy).into(buttonLevel5);
        Glide.with(this).load(R.drawable.background_road).into(buttonLevel6);
        Glide.with(this).load(R.drawable.background_wood).into(buttonLevel7);
        Glide.with(this).load(R.drawable.background_pacman).into(buttonLevel8);
    }

}