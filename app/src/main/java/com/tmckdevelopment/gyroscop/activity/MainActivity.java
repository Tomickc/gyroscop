package com.tmckdevelopment.gyroscop.activity;

import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;

import com.tmckdevelopment.gyroscop.R;
import com.tmckdevelopment.gyroscop.fragments.MenuFragment;
import com.tmckdevelopment.gyroscop.fragments.PlayTableFragment;
import com.tmckdevelopment.gyroscop.objects.Barrier;
import com.tmckdevelopment.gyroscop.objects.Level;

public class MainActivity extends AppCompatActivity {

    private Level level;
    private String currentFragment;
    private int currentLevel;
    private boolean hasWon;
    private int width, height;
    private Barrier barrier;


    public boolean hasWon() {
        return hasWon;
    }

    public void setHasWon(boolean hasWon) {
        this.hasWon = hasWon;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }


    public void setLevel(Level level) {
        this.level = level;
    }


    public Level getLevel() {
        return level;
    }

    public void setCurrentFragment(String currentFragment) {
        this.currentFragment = currentFragment;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public void setLevel1() {
        level = new Level();
        level.setBackground(R.drawable.background_paper);
        level.setBallRes(R.drawable.white_ball);
        level.setHoleRes(R.drawable.paper_hole);

        setCurrentLevel(1);
        setLevel(level);
        showFragment(PlayTableFragment.newInstance(), PlayTableFragment.TAG, false);
    }

    public void setLevel2() {
        level = new Level();
        level.setBackground(R.drawable.background_aqua);
        level.setBallRes(R.drawable.bubble);
        level.setHoleRes(R.drawable.aqua_hole);

        level.setShowInForeground(true);

        barrier = new Barrier();
        barrier.setBarrierRes(R.drawable.shark);
        barrier.setSpeed(250, height, width);
        barrier.moveLeft(true);
        barrier.setBarrierPosition(width, height / 2);


        level.getBarriers().add(barrier);

        setCurrentLevel(2);
        setLevel(level);
        showFragment(PlayTableFragment.newInstance(), PlayTableFragment.TAG, false);
    }

    public void setLevel3() {
        level = new Level();
        level.setBackground(R.drawable.background_slither);
        level.setBallRes(R.drawable.slither_ball);
        level.setHoleRes(R.drawable.slither_hole);

        level.setShowInForeground(true);

        barrier = new Barrier();
        barrier.setBarrierRes(R.drawable.snake);
        barrier.setSpeed(40, height, width);
        barrier.moveDown(true);
        barrier.setBarrierPosition(width / 3, height / 2);
        barrier.sensitive(true);

        level.getBarriers().add(barrier);

        setCurrentLevel(3);
        setLevel(level);
        showFragment(PlayTableFragment.newInstance(), PlayTableFragment.TAG, false);
    }


    public void setLevel4() {

        level = new Level();
        level.setBackground(R.drawable.background_grass);
        level.setBallRes(R.drawable.golf_ball);
        level.setHoleRes(R.drawable.golf_hole);

        level.setShowInForeground(false);

        for (int i = 0; i < 6; i++) {

            switch (i) {
                case 0:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.golf_barrier_one);
                    barrier.setBarrierPosition(90, 90);
                    break;
                case 1:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.golf_barrier_two);
                    barrier.setBarrierPosition(width / 12, (height / 4));
                    break;
                case 2:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.golf_barrier_three);
                    barrier.setBarrierPosition((int) (width / 1.5), height / 4);
                    break;
                case 3:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.golf_barrier_two);
                    barrier.setBarrierPosition((int) (width / 3), height / 2);
                    break;
                case 4:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.golf_barrier_three);
                    barrier.setBarrierPosition(-20, (int) (height / 1.5));
                    break;
                case 5:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.golf_barrier_one);
                    barrier.setBarrierPosition((int) (width / 1.7), (int) (height / 2.5));
                    break;
            }
            level.getBarriers().add(barrier);
        }


        setCurrentLevel(4);
        setLevel(level);
        showFragment(PlayTableFragment.newInstance(), PlayTableFragment.TAG, false);
    }

    public void setLevel5() {
        level = new Level();
        level.setBackground(R.drawable.background_flappy);
        level.setBallRes(R.drawable.flappy_bird);
        level.setHoleRes(R.drawable.flappy_hole);

        level.setShowInForeground(true);

        BitmapDrawable bd = (BitmapDrawable) this.getResources().getDrawable(R.drawable.column_up);

        for (int i = 0; i < 4; i++) {

            switch (i) {
                case 0:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.column_down);
                    barrier.setBarrierPosition(width / 6, 0);
                    barrier.sensitive(true);
                    break;
                case 1:
                    barrier = new Barrier();
                    barrier.sensitive(true);
                    barrier.setBarrierRes(R.drawable.column_up);
                    barrier.setBarrierPosition(width / 6, (int) (height / 1.45));
                    break;

                case 2:
                    barrier = new Barrier();
                    barrier.sensitive(true);
                    barrier.setBarrierRes(R.drawable.column_down);
                    barrier.setBarrierPosition(width / 2, -(height / 4));
                    break;
                case 3:
                    barrier = new Barrier();
                    barrier.sensitive(true);

                    barrier.setBarrierRes(R.drawable.column_up);
                    barrier.setBarrierPosition(width / 2, (height - (bd.getBitmap().getHeight() / 4)));
                    break;


            }
            level.getBarriers().add(barrier);
        }

        setCurrentLevel(5);
        setLevel(level);
        showFragment(PlayTableFragment.newInstance(), PlayTableFragment.TAG, false);
    }

    public void setLevel6() {
        level = new Level();
        level.setBackground(R.drawable.background_road);
        level.setBallRes(R.drawable.pneu);
        level.setHoleRes(R.drawable.road_hole);

        level.setShowInForeground(true);
        for (int i = 0; i < 2; i++) {

            switch (i) {
                case 0:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.car_one);
                    barrier.setSpeed(70, height, width);
                    barrier.moveUp(true);
                    barrier.setBarrierPosition((int) (width - (width / 2.5)), height);
                    break;
                case 1:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.car_two);
                    barrier.setSpeed(60, height, width);
                    barrier.moveDown(true);

                    barrier.setBarrierPosition(width / 10, height / 2);
                    break;
            }
            level.getBarriers().add(barrier);
        }

        setCurrentLevel(6);
        setLevel(level);
        showFragment(PlayTableFragment.newInstance(), PlayTableFragment.TAG, false);
    }

    public void setLevel7() {
        level = new Level();
        level.setBackground(R.drawable.background_wood);
        level.setBallRes(R.drawable.wood_cut);
        level.setHoleRes(R.drawable.wood_hole);

        level.setShowInForeground(true);

        for (int i = 0; i < 4; i++) {

            switch (i) {
                case 0:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.wood_barier_one);
                    barrier.moveLeft(true);
                    barrier.setSpeed(100, height, width);
                    barrier.setBarrierPosition(0, height / 8);
                    break;
                case 1:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.wood_barier_two);
                    barrier.moveRight(true);
                    barrier.setSpeed(85, height, width);
                    barrier.setBarrierPosition(width, (int) (height / 3.5));
                    break;
                case 2:
                    barrier = new Barrier();
                    barrier.setSpeed(70, height, width);
                    barrier.setBarrierRes(R.drawable.wood_barier_three);
                    barrier.setBarrierPosition(0, height / 2);
                    barrier.moveLeft(true);
                    break;
                case 3:
                    barrier = new Barrier();
                    barrier.setSpeed(60, height, width);
                    barrier.moveRight(true);
                    barrier.setBarrierRes(R.drawable.wood_barier_four);
                    barrier.setBarrierPosition(width, (int) (height / 1.5));
                    break;
            }
            level.getBarriers().add(barrier);
        }


        setCurrentLevel(7);
        setLevel(level);
        showFragment(PlayTableFragment.newInstance(), PlayTableFragment.TAG, false);
    }

    public void setLevel8() {
        level = new Level();
        level.setBackground(R.drawable.background_pacman);
        level.setBallRes(R.drawable.pacman);
        level.setHoleRes(R.drawable.pacmant_hole);

        level.setShowInForeground(true);

        for (int i = 0; i < 4; i++) {

            switch (i) {
                case 0:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.ghost_one);
                    barrier.moveLeft(true);
                    barrier.setSpeed(100, height, width);
                    barrier.setBarrierPosition(0, height / 8);
                    break;
                case 1:
                    barrier = new Barrier();
                    barrier.setBarrierRes(R.drawable.ghost_two);
                    barrier.moveRight(true);
                    barrier.setSpeed(70, height, width);
                    barrier.setBarrierPosition(width, (int) (height / 1.5));
                    break;
                case 2:
                    barrier = new Barrier();
                    barrier.setSpeed(100, height, width);
                    barrier.setBarrierRes(R.drawable.ghost_three);
                    barrier.setBarrierPosition(width / 4, height);
                    barrier.moveUp(true);
                    break;
                case 3:
                    barrier = new Barrier();
                    barrier.setSpeed(70, height, width);
                    barrier.moveDown(true);
                    barrier.setBarrierRes(R.drawable.ghost_four);
                    barrier.setBarrierPosition(width / 2, 0);
                    break;
            }
            level.getBarriers().add(barrier);
        }


        setCurrentLevel(8);
        setLevel(level);
        showFragment(PlayTableFragment.newInstance(), PlayTableFragment.TAG, false);
    }

    @Override
    public void onBackPressed() {
        if (currentFragment.equals("menu_fragment")) super.onBackPressed();
        showFragment(MenuFragment.newInstance(), MenuFragment.TAG, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        showFragment(MenuFragment.newInstance(), MenuFragment.TAG, false);
    }

    public void showFragment(Fragment fragment, String tag, boolean backStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, fragment, tag);
        setCurrentFragment(tag);
        if (backStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }
}
