package com.tmckdevelopment.gyroscop.fragments;

/**
 * Created by Tomas on 14.07.2016.
 */

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tmckdevelopment.gyroscop.R;
import com.tmckdevelopment.gyroscop.activity.MainActivity;

public class FinalFragment extends BaseFragment {
    //Set fragment's TAG
    public static final String TAG = "final_fragment";

    private TextView time, succes;
    private Button restart, nextLevel, menu;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_final, container, false);

        defineViews(view);
        checkWin();


        nextLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (((MainActivity) getActivity()).getCurrentLevel()) {

                    case 1:
                        ((MainActivity) getActivity()).setLevel2();
                        break;
                    case 2:
                        ((MainActivity) getActivity()).setLevel3();
                        break;
                    case 3:
                        ((MainActivity) getActivity()).setLevel4();
                        break;
                    case 4:
                        ((MainActivity) getActivity()).setLevel5();
                        break;
                    case 5:
                        ((MainActivity)getActivity()).setLevel6();
                        break;
                    case 6:
                        ((MainActivity) getActivity()).setLevel7();
                        break;
                    case 7:
                        ((MainActivity)getActivity()).setLevel8();
                        break;
                }
            }
        });

        return view;
    }

    private void checkWin() {
        if (!(((MainActivity) getActivity()).hasWon())) {
            succes.setText("DOTKL JSI SE PŘEKÁŽKY!");
            succes.setTextColor(Color.RED);
            nextLevel.setVisibility(View.INVISIBLE);
        } else succes.setTextColor(Color.GREEN);
    }

    private void defineViews(View view) {
        time = (TextView) view.findViewById(R.id.textViewTime);
        restart = (Button) view.findViewById(R.id.buttonRestart);
        nextLevel = (Button) view.findViewById(R.id.buttonNextLevel);
        menu = (Button) view.findViewById(R.id.buttonMenu);
        succes = (TextView) view.findViewById(R.id.succes);


        time.setText((((MainActivity) getActivity()).getLevel().getTime()) + " vteřin");

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFragment(PlayTableFragment.newInstance(), PlayTableFragment.TAG, false);
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFragment(MenuFragment.newInstance(), MenuFragment.TAG, false);
            }
        });
    }

    //Create new instance of this fragment
    public static FinalFragment newInstance() {
        FinalFragment fragment = new FinalFragment();
        return fragment;
    }
}