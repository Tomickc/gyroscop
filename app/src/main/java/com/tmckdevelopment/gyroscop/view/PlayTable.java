package com.tmckdevelopment.gyroscop.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.View;

import com.tmckdevelopment.gyroscop.activity.MainActivity;
import com.tmckdevelopment.gyroscop.objects.Level;

/**
 * Created by Tomas on 12.07.2016.
 */
public class PlayTable extends View implements SensorEventListener {

    private Sensor accelerometer;
    private SensorManager sensorManager;
    private boolean showBackground = true;
    private static int BALL_SIZE = 33;
    private Level level;
    private Context context;
    private int ballSize, barrierSize;
    private Bitmap ballImage, holeImage, barrierImage;

    public float ballX = 0, ballY = 0;

    public PlayTable(Context context) {
        super(context);

        this.context = context;
        level = ((MainActivity) context).getLevel();

        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        if (showBackground) {
            ballSize = ((canvas.getWidth() + canvas.getHeight()) / BALL_SIZE);

            level.setBarriers(context,canvas);

            holeImage = (Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), level.getHoleRes()), ballSize * 2, ballSize * 2, false));
            ballImage = (Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), level.getBallRes()), ballSize, ballSize, false));
            showBackground = false;
        }

        drawCorners(canvas);
        if(level.isShowInForeground()){

            level.drawHole(canvas, ballX, ballY, ballSize, context, holeImage);
            level.drawBall(canvas, ballImage, ballX, ballY);
            level.drawBarriers(canvas, ballX, ballY, context,ballSize);

        }


        if(!level.isShowInForeground()){


            level.drawBarriers(canvas, ballX, ballY, context,ballSize);
            level.drawHole(canvas, ballX, ballY, ballSize, context, holeImage);
            level.drawBall(canvas, ballImage, ballX, ballY);
        }



        invalidate();
    }

    private void drawCorners(Canvas canvas) {
        if (ballX < -(ballSize / (ballSize / 10))) ballX = -(ballSize / (ballSize / 10));
        if (ballX > (canvas.getWidth() - (ballSize) + (ballSize / (ballSize / 10))))
            ballX = (canvas.getWidth() - (ballSize) + (ballSize / (ballSize / 10)));
        if (ballY < -(ballSize / (ballSize / 10))) ballY = -(ballSize / (ballSize / 10));
        if (ballY > canvas.getHeight() - (ballSize) + (ballSize / (ballSize / 10)))
            ballY = canvas.getHeight() - (ballSize) + (ballSize / (ballSize / 10));
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        ballX += ((-1) * (sensorEvent.values[0]));
        ballY += (sensorEvent.values[1]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }
}
